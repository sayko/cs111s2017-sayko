//**********************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Ryan Sayko
// CMPSC 111 Spring 2017
// Practical 5
// Date: Feb 26 2017
//
// Purpose: This program produces a MadLib esque style
//		of a story using inputs from the user.
//**********************************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;
import java.text.NumberFormat;		


public class MadLib
{
	//---------------------------------
	// main method: program execution begins here
	//---------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Ryan Sayko\nPractical 5\n" + new Date() + "\n");
		NumberFormat fmt1 = NumberFormat.getCurrencyInstance();

		// Variable dictionary:
		String noun, adjective, verb;
		int num1, num2, num3;
		Scanner scan = new Scanner(System.in);

		System.out.println("Enter a noun that describes a person's occupation:");
		noun = scan.nextLine();
		System.out.println("Enter an adjective:");
		adjective = scan.nextLine();
		System.out.println("Enter a verb:");
		verb = scan.nextLine();
		System.out.println("Enter an integer number:");
		num1 = scan.nextInt();
		System.out.println("And finally enter another interger number:");
		num2 = scan.nextInt();

		num3 = num1 * num1 * num1 * num1;

		System.out.println("A " + noun + " walks into a bar and orders a drink.\nOnce the " + noun + " drinks the beverage, he/she realizes it is too " + adjective + ".\nSo he/she decides to " + verb + " and go home.\nAfter walking for " + num1 + " miles, he/she hitches a ride with a " + num2 + " year old man.\nWhen the " + noun + " goes back home, he/she decides \nto go back to school the next day \nand get a job that pays a " + fmt1.format(num3) + " salary \nwhile living a happy life. \nThe end."); 
		
		
	}
}
