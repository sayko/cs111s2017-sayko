
//**********************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Ryan Sayko
// CMPSC 111 Spring 2017
// Practical #3
// Date: Feb 3 2017
//
// Purpose:	This program calculates the number of seconds in a week through 
//		converting days per week into seconds per week. The program also
//		calculates the number of seconds in January, February, and the sum of both January
//		and February of this year based on the number of days in January and February. Finally,
//		The program calculates the average amount of seconds over the two
//		months. This is the bugged version.
//**********************************************************
import java.util.Date; // needed for printing today's date


public class practical03;
{
	//---------------------------------
	// main method: program execution begins here
	//---------------------------------

	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Ryan Sayko\nLab #2\n" + new Date() + "\n");

		// Variable dictionary:
		int SecInMinute = 60;	// Number of seconds in a minute
		int MinInHour = 60; 	// Number of minutes in an hour
		int HourInDay = 24; 	// Number of hours in a day
		int DayInWeek = 7;	// Number of days in a week
		char SecInWeek;		// Number of seconds in a week
		int SecInJan;		// Number of seconds in January
		int DayInJan = 3;	// Number of days in January
		int SecInFeb;		// Number of seconds in February
		int DayInFeb = 28;	// Number of days in February in 2017
		int TotalSec;		// Total amount of seconds from January and February
		int AveSec;		// Average seconds of January and February

		// Computes the number of seconds in a week, the number of 
		// seconds in Jan, number of seconds in Feb, the total 
		// number of seconds in Jan and Feb, and the average 
		// number of seconds of the two months. 
		SecInWeek = SecInMinute * MinInHour * HourInDay * 
		+ DayInWeek;
		SecInJan = SecInMinute + MinInHour + HourInDay + DayInJan;
		SecInFeb = SecInMinute * MinInHour * HourInDay * DayInFeb;
		TotalSec = SecInJan + SecInFeb;
		AveSec = TotalSec/2;

		// Print out values
		System.out.println("Number of seconds in a minute: " + SecInMinute);
		System.out.println("Number of minutes in an hour: " + MinInHour);
		System.out.println("Number of hours in a day: " + HourInDay);
		System.out.println("Number of days in a week: " + DayInWeek);
		System.out.println("Number of seconds in a week: " + SecInWeek);
		System.out.println("Number of days in January: " + DayInJan);
		System.out.println("Number of days in February: " + DayInFeb);
		System.out.println("Number of seconds in January: " + SecInJan);
		System.out.println("Number of seconds in February: " + SecInFeb);
		System.out.print("Total number of seconds in January and February: " + TotalSec);
		System.out.println("Average number of seconds over the two months: " + AveSec);
	}
}
