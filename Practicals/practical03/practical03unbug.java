// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Meredith Dreistadt
// CMPSC 111 Spring 2017
// Lab # 3
// Date: February 2, 2017
//
// Purpose: Calculate tip

import java.util.Scanner;

public class practical03bugged
{
	public static void main(String[] args)
	{
		String name;
		String greeting;
		double bill;
		double percentage;
		double tip;
		double total_bill;
		double share;
		double pay;
		int split;

		Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter your name: ");
		name = scan.nextLine();

		System.out.println("Welcome to Tip Calculator " + name);
		
		System.out.println("Please enter bill amount: ");
		bill = scan.nextDouble();

		System.out.println("Please enter the tip percentage between 0 and 100: ");
		percentage = scan.nextDouble();

		tip = (percentage * bill) / 100;

		total_bill = bill + tip;

		System.out.println("Your total bill is: " + total_bill);
	
		System.out.println("Please enter the number of people splitting the bill: ");
		split = scan.nextInt();

		share = total_bill / split;

		System.out.println("Each person should pay: " + share);

		System.out.println("Thank you and have a great day!");
			
	}
}
