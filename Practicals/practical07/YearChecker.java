//*****************************
// CMPSC 111
// Practical 8
// October 30, 2015
//
// Purpose: determine if user's year input is a leap year,
// cicada brood II emergence year, or a peak sunspot year.
//*****************************

import java.util.Scanner;

public class YearChecker
{
    //Create instance variables
    int year;

    //Create a constructor
    public YearChecker(int y)
    {
        year = y;
    }

    // a method that checks if the user's input year is a leap year
    public boolean isLeapYear(int year)
    {
        // TO DO: complete
	if (year % 4 == 0 && year % 400 !=0) {
		//y = true;
		//System.out.println(year + " is a leap year.");
	}
	else {
		//y = false;
		//System.out.println(year + " is not a leap year.");
    }
	//return year;

    // a method that checks if the user's input year is a cicada year
    public boolean isCicadaYear(year)
    {
        // TO DO: complete
	if ((year-2013) % 17 == 0){
		//y = true;
		//System.out.println(year + " is an emergence year for Brood II of 17-year cicadas);
	}
	else {
		//y = false;
		//System.out.println(year + " is not an emergence year for Brood II of 17-year cicadas);
	}
	//return y;
    }

    // a method that check if the user's input year is a sunspot year
    public boolean isSunspotYear(year)
    {
        // TO DO: complete
	if ((year-2013) % 1 == 0){
		//y = true;
		//System.out.println(year + " is a peak sunspot year");
	}
	else {
		//y = false;
		//System.out.println(year + " is not a peak sunspot year");
    }
	//return y;
}
