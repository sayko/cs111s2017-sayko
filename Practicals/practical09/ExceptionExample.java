// This example is derived from a Java source code snippet found at:
// http://pages.cs.wisc.edu/~hasti/cs368/JavaTutorial/NOTES/Exceptions.html

public class ExceptionExample {

  static void throwsExceptions(int k, int[] A, String S) {
    int j = 1 / k;
    int len = A.length + 1;
    char c;

    try {
      c = S.charAt(0);
      if (k == 10) j = A[3];
    }
    catch (ArrayIndexOutOfBoundsException ex) {
      System.out.println("Array error");
	ex.printStackTrace();
    }
    catch (ArithmeticException ex) {
      System.out.println("Arithmetic error");
	ex.printStackTrace();
    }
    catch (NullPointerException ex) {
      System.out.println("Null pointer");
	ex.printStackTrace();
    }
    catch (NumberFormatException ex) {
	System.out.println("Number format error");
	ex.printStackTrace();
    }
    finally {
      System.out.println("In the finally clause");
    }
    System.out.println("After the try block");
  }

  public static void main(String[] args) {
    int[] X = {0,1,2};
      //throwsExceptions(0, X, "hi");
	// 	----------Runtime Error:----------
	//	Exception in thread "main" java.lang.ArithmeticException: / by zero
	//	at ExceptionExample.throwsExceptions(ExceptionExample.java:7)
	//	at ExceptionExample.main(ExceptionExample.java:32)
	//	----------------------------------
	//	Line 7 attempts to divide 1 by zero. Since throwsExceptions changes the value of k to zero at line 32, it will come up with an Arithmetic Exception error. 
		
    // throwsExceptions(10, X, "");
	// 	----------Runtime Error:----------
	//	Exception in thread "main" java.lang.StringIndexOutOfBoundsException: String index out of range: 0
	//	at java.lang.String.charAt(String.java:658)
	//	at ExceptionExample.throwsExceptions(ExceptionExample.java:12)
	//	at ExceptionExample.main(ExceptionExample.java:33)
	//	----------------------------------
	//	Line 12 attempts to locate an index value that is out of the range of the declared array. 
    // throwsExceptions(10, X, "bye");
	//	----------Runtime Error:----------
	//	Array error
	//
	//	java.lang.ArrayIndexOutOfBoundsException: 3
	//	at ExceptionExample.throwsExceptions(ExceptionExample.java:13)
	//	at ExceptionExample.main(ExceptionExample.java:49)
	//
	//	In the finally clause
	//	After the try block
	//	----------------------------------
	//	
    // throwsExceptions(10, X, null);
	// 	----------Runtime Error:----------
	//	Null pointer
	//
	//	java.lang.NullPointerException
	//	at ExceptionExample.throwsExceptions(ExceptionExample.java:12)
	//	at ExceptionExample.main(ExceptionExample.java:62)
	//	
	//	In the finally clause
	//	After the try block
	//	----------------------------------
	throwsExceptions(3, null, "hi");
	//	----------Runtime Error:----------
	//	Exception in thread "main" java.lang.NullPointerException
	//	at ExceptionExample.throwsExceptions(ExceptionExample.java:8)
	//	at ExceptionExample.main(ExceptionExample.java:78)
	//	----------------------------------
  }

}
