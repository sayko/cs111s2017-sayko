//**********************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Ryan Sayko
// CMPSC 111 Spring 2017
// Practical 6
// Date: Mar 31 2017
//
// Purpose: This program generates a random number between 1 and 100. After
//		generating the random number, it prompts the user to guess
//		the right number. The program then enters a while loop until
//		the user guesses the right number. The program also uses an if 
//		and else if statement to help the user guess the right number. Once
//		the user guesses the right answer, the while loop condition is broken
//		and the program congradulates the user for guessing the right number.
//		The program also counts the number of guesses the user makes!
//**********************************************************
import java.util.Date; 		// needed for printing today's date
import java.util.Scanner;	// needed to use the scanner
import java.util.Random;	// needed to generate a random number


public class GuessMyNumber
{
	//---------------------------------
	// main method: program execution begins here
	//---------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Ryan Sayko\nPractical 6\n" + new Date() + "\n");
	
	//---------------------------------
	// initialize scanner, random number generator and define variables
	//---------------------------------
	Scanner scan = new Scanner(System.in);
	Random number= new Random();
	int num;
	int guess;
	int tries;
	
	//---------------------------------
	// generate the random number and add 1 to put in range of 1-100
	//---------------------------------
	num = number.nextInt(100);
	num++;
	tries = 1;

	//---------------------------------
	// start guessing game
	//---------------------------------
	System.out.println("Welcome to the guessing game! Guess what integer number has been randomly generated between 1 and 100!");
	guess = scan.nextInt();		// scans first guess
	while (guess != num)
	{
		if (guess > num)	// if the guess is too high, the program will say so
		{
			System.out.println("That number is too high!");
			guess = scan.nextInt();
			tries++;
		}
		else if (guess < num)	// if the guess is too low, the program will say so
		{
			System.out.println("That number is too low!");
			guess = scan.nextInt();
			tries++;
		}
	}
	System.out.println("Congratulations! You guessed the right number in " + tries + " attempts!");
}
}
