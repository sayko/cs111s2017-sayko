
//**********************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Ryan Sayko
// CMPSC 111 Spring 2017
// Practical 02
// Date: Jan 27 2017
//
// Purpose: 	This program prints an output that represents a 
//		sort of art comprised of characters. 
//**********************************************************
import java.util.Date; // necessary for printing today's date


public class PrintText
{
	//---------------------------------
	// main method: program execution begins here
	//---------------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Ryan Sayko\nPractical 02\n" + new Date() + "\n");

		// Print out 
		System.out.println("    ----     -----  \\\\ \\|   |//    //_____________		");
		System.out.println("   /---\\\\  //     \\\\ \\\\ |   |/    //   /    /     \\	");
		System.out.println("  /|   |\\ /|/  -  \\|\\ \\\\|   |/   //|  /    /       \\	");
		System.out.println("  /|      //| | | |\\\\  \\|   |/  // | /    /  ----   \\	");
		System.out.println("   \\---\\  //|  -  |\\\\   |___|  /   |/    /  /    \\   \\	");
		System.out.println("    ---\\| /||_____||\\   |     |          |  |    |   |	");
		System.out.println("       |\\ |||_____|||   |     |          \\  \\    /   /	");
		System.out.println(" \\\\    |/ |||     |||   |     |    |\\    \\   ----   /	");
		System.out.println("   \\-- /  |||     |||   |     |    | \\    \\        /	");
		System.out.println("    ---   |||     |||   |_____|____|  \\____\\______/    	");
	}
}
