//**********************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Ryan Sayko
// CMPSC 111 Spring 2017
// Practical #4
// Date: Feb 10 2017
//
// Purpose:	This program is intended to create a picture of two colors.
//		The color on the left of the output represents a color 
//		created from user input values of red, green and blue. The 
//		color on the right represents the complementary color of the
//		user input color. The complementary colors are created by taking
//		the value of 255 (max limit) and subtracting the value of the 
//		user input color value. 
//**********************************************************

import java.awt.*;
import javax.swing.JApplet;

public class PaintDrawingCanvas extends JApplet {

  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
  public void paint(Graphics page) {
    // create the color based on the input values from the user
    Color userColor = new Color(DisplayDrawingCanvas.redValue,
        DisplayDrawingCanvas.greenValue, DisplayDrawingCanvas.blueValue);

    // fill the first half (left-to-right) with the user's color
    // make a call to page.fillRect with the correct parameters
    page.setColor(userColor);
	page.fillRect(0,0,300,400);

    // calculate the "complementary" color of the provided color
    // and then create a new Color object called userComplementaryColor.
    // Refer to notes in the practical assignment sheet about this calculation
    // (add your own calculation to replace the value of null)
    Color userComplementaryColor = new Color(DisplayDrawingCanvas.redCompValue, DisplayDrawingCanvas.greenCompValue, DisplayDrawingCanvas.blueCompValue);

    // fill the second half (left-to-right) with the complement of the user's color
    // make a call to page.fillRect with the correct parameters
    page.setColor(userComplementaryColor);
	page.fillRect(300,0,300,400);

  }
}
