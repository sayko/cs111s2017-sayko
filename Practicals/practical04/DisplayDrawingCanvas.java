//**********************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Ryan Sayko
// CMPSC 111 Spring 2017
// Practical #4
// Date: Feb 10 2017
//
// Purpose: 	This program sets up the drawing canvas used 
//		by PaintDrawingCanvas. Creates values of red, green
//		and blue values as well as the complementary red,
//		green and blue values. 
//**********************************************************

import javax.swing.*;
import java.util.Date;
import java.util.Scanner;

public class DisplayDrawingCanvas {

  // declare variables that can store the user's color values
  public static int redValue;
  public static int greenValue;
  public static int blueValue;
	public static int redCompValue;
	public static int greenCompValue;
	public static int blueCompValue;

  // define the HEIGHT and WIDTH of the graphic
  public static final int WIDTH = 600;
  public static final int HEIGHT = 400;

  // solicit input from the user on the rectangle's color
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.print("Input the Red Value: ");
    redValue = scan.nextInt();
	redCompValue = 255-redValue;

    System.out.print("Input the Green Value: ");
    greenValue = scan.nextInt();
	greenCompValue = 255-greenValue;

    System.out.print("Input the Blue Value: ");
    blueValue = scan.nextInt();
	blueCompValue = 255-blueValue;

    JFrame window = new JFrame("Ryan C. Sayko " + new Date());

    // Add the drawing canvas and do necessary things to
    // make the window appear on the screen!
    window.getContentPane().add(new PaintDrawingCanvas());
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setVisible(true);
    window.setSize(WIDTH, HEIGHT);
  }
}

