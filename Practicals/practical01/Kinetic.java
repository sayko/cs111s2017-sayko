//*********************************************************
// Ryan Sayko
// Practical 01
//
// This program is supposed to calculate the kinetic energy
// of an object with a mass moving at a velocity. Should be
// modeled by K= (1/2)mv^2. Upon running, the program calculates
// the final velocity to by 24*24 + 24 = 600m/s. It is unclear 
// how this program is trying to calculate the final velocity. 
//*********************************************************

import java.lang.Math;	// Imports library of language in Math

// Declares Kinetic to be a public class
public class Kinetic {

	// Declares kinetic and mass to be integers, set up parameters for velocity
	// and velocity_squared, to be equal to 0. 
  public static String computeVelocity(int kinetic, int mass) {
    int velocity_squared = 0;
    int velocity = 0;
    StringBuffer final_velocity = new StringBuffer();

	// Prints out current values of velocity and velocity_squared, already equal to 0.
    System.out.println("F V: " + velocity);
    System.out.println("F V squared: " + velocity_squared);

	// If mass is not equal to zero then the program should calculate the new value
	// of velocity_squared. Since no mass has been declared, it has a value of 0.
    if( mass != 0 ) {
      velocity_squared = 3 * (kinetic / mass);
      velocity = (int)Math.sqrt(velocity_squared);
      final_velocity.append(velocity);
    }

    else {
      final_velocity.append("Undefined");
    }

    System.out.println("L V: " + velocity);
    System.out.println("L V squared: " + velocity_squared);

    return final_velocity.toString();
  }

	// Prints value of velocity. 
  public static void main(String[] args) {
    System.out.println("Calling the computeVelocity method!");
    String velocity = computeVelocity(1000,5);
    System.out.println("The velocity is: " + velocity);
  }

}
