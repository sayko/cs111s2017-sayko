# README #

Hello! My name is Ryan Sayko and I am a physics major and music minor. 
I am a senior at Allegheny College looking to go to graduate school in the Fall of 2017. 
I play percussion/drums for numerous musical groups around Meadville and back home in New York. 
I like to play numerous different genres (jazz, rock, alternative in general). 

I am taking CS 111 for the natural lab distribution requirement. This class offers useful info 
that could help me out in the future in case I decide to go into computational work in graduate school. 
I want to be able to do computational work on my laptop easily by the end of this semester.