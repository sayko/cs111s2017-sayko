
//**********************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Ryan Sayko
// CMPSC 111 Spring 2017
// Lab #3
// Date: Feb 2 2017
//
// Purpose: The purpose of this lab is to calculate the total amount of money
//		to pay in order to cover the bill of an order. The program takes
//		inputs of the user's name, the bill they have to pay before tipping,
//		the tip percentage and how many people are splitting the bill. The 
//		program then calculates the total amount of money to pay in total 
//		and per person. For simplicity, the program uses an imported format 
//		displaying the US currency in dollars. 
//**********************************************************
import java.util.Date; 			// Needed to print today's date.
import java.util.Scanner;		// Needed to take inputs from user.
import java.text.NumberFormat;		// Needed for US currency format.


public class Lab3
{
	//---------------------------------
	// Program execution begins here.
	//---------------------------------
	public static void main(String[] args)
	{
		// Prints the name, Lab number, date and time.
		System.out.println("Ryan Sayko\nLab #3\n" + new Date() + "\n");

		// Variable Declarations and number format declaration.
		String name;
		float Bill;		// Original bill amount.
		float PercentTip;	// Percent tip.
		float TipAmt;		// Tip amount being added onto bill.
		float total_bill;	// Total amount of bill including tip.
		int People;		// Number of people splitting the total bill.
		float PeopleShare;	// Amount of money split per person paying for the bill. 
		NumberFormat fmt1 = NumberFormat.getCurrencyInstance();

		// Create scanner object to take inputs from user.
		Scanner scan = new Scanner(System.in);

		// Introduce the user to Calculator, asks for input of name,
		// bill amount and how much to tip. 
		System.out.println("Welcome to the tip calculator! Enter your name:");
		name = scan.nextLine();
		System.out.println("Hello " + name + "!\nHow much do you have to pay?");
		Bill = scan.nextFloat();
		System.out.println("Enter what percentage you want to tip, between 0 and 100%:");
		PercentTip = scan.nextFloat();
		
		// Calculate total value of the bill, including tip. Then program prints
		// how much the original bill is, the tip amount and the total bill. 
		// Then the program inputs how many people are splitting the bill,
		// and calculates the amount of money per person required to satisfy 
		// the bill. 
		TipAmt = (PercentTip/100)*Bill;
		total_bill = Bill + TipAmt;
		System.out.println("Your original bill is " + fmt1.format(Bill) + ",\nThe tip amount is " + fmt1.format(TipAmt) + ",\nand your total bill is " + fmt1.format(total_bill) + ".\nHow many people are splitting the bill?");
		People = scan.nextInt();
		PeopleShare = total_bill/People;
		System.out.println("There are " + People + " people sharing the bill.\nEach person should pay " + fmt1.format(PeopleShare) + ".");

		// Polite message to the user, thanking them for using the Calculator program. 
		System.out.println("Thank you for using this tip calculator! Bye!");
	}
}
