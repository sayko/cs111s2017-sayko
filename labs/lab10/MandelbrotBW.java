//**********************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts, other than
//		the example programs taken from http://jonisalonen.com/2013/lets-draw-the-mandelbrot-set.
//		
// Ryan Sayko
// CMPSC 111 Spring 2017
// Lab #10
// Date: April 6 2017
//
//	Purpose: This program designs fractals based on iterations from an 
//	imaginary number. And imaginary number includes the letter "i" to 
//	represent imaginary. Hence, this fractal generator is based on 
//	a parametric axis. The program sets the parameters of the output picture,
//	such as width, height and maximum number of iterations. Based on the maximum
//	iterations, values of color names, increments of iterations, and color value 
//	increment, the resulting pictures running time and output picture will change. 
//	The result file of this program is in black and white.
//**********************************************************

import java.awt.image.BufferedImage;	// Create a space for an image file
import java.io.File;			// Output the resulting file
import javax.imageio.ImageIO;		// Output the resulting image

// NOTE: You do not need to understand all of the working details associated with this program.
// Instead, you should develop a basic understanding of how it works. Please consult with a
// teaching assistant, tutor, or a course instructor if there are parts of the program that you
// do not intuitively understand.

public class MandelbrotBW {
    public static void main(String[] args) throws Exception {
        int width = 1920, height = 1080, max = 1000;				// Sets height and width of picture
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        int black = 0x000000, white = 0xFFFFFF;					// Assigns color values to variables

        for (int row = 0; row < height; row++) {				// Starts for loop to shade in space via each row
            for (int col = 0; col < width; col++) {				// Starts for loop to shade in each column. Once each column
                double c_re = (col - width/2)*4.0/width;			// is filled in, the loop goes to next row. 
                double c_im = (row - height/2)*4.0/width;
                double x = 0, y = 0;
                int iterations = 0;
                while (x*x+y*y < 4 && iterations < max) {			// Starts while loop to explain each iteration
                    double x_new = x*x-y*y+c_re;
                    y = 2*x*y+c_im;						// Y value is imaginary
                    x = x_new;							// X value is real
                    iterations++;						// Iterations counter increased by 1
                }
                if (iterations < max) image.setRGB(col, row, white);		// For each iteration that is below 1000, space is white
                else image.setRGB(col, row, black);				// If iteration is equal to or above 1000, space is black
            }
        }
        ImageIO.write(image, "png", new File("mandelbrot-bw.png"));		// Writes image result to png file
    }
}
