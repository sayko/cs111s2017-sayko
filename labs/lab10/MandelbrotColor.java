//**********************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts, other than
//		the example programs taken from http://jonisalonen.com/2013/lets-draw-the-mandelbrot-set.
//		
// Ryan Sayko
// CMPSC 111 Spring 2017
// Lab #10
// Date: April 6 2017
//
//	Purpose: This program designs fractals based on iterations from an 
//	imaginary number. And imaginary number includes the letter "i" to 
//	represent imaginary. Hence, this fractal generator is based on 
//	a parametric axis. The program sets the parameters of the output picture,
//	such as width, height and maximum number of iterations. Based on the maximum
//	iterations, values of color names, increments of iterations, and color value 
//	increment, the resulting pictures running time and output picture will change. 
//**********************************************************
import java.awt.Color;				// Insert a class of spaces designated for colors
import java.io.File;				// Output a resulting file
import java.awt.image.BufferedImage;		// Insert a library of images to use
import javax.imageio.ImageIO;			// Output an image file

// NOTE: You do not need to understand all of the working details associated with this program.
// Instead, you should develop a basic understanding of how it works. Please consult with a
// teaching assistant, tutor, or a course instructor if there are parts of the program that you
// do not intuitively understand. 

public class MandelbrotColor {					// Create a class named after file

    public static void main(String[] args) throws Exception {
        int width = 1920, height = 1080, max = 1000;		// Designate a width and height of output picture
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        int black = 0;						// Designate a value for the color black
        int[] colors = new int[max];
        // starting point: i = 0
        // termination condition: i<max
        // advancement strategy: i++
        for (int i = 0; i<max; i++) {
            colors[i] = Color.HSBtoRGB(i/1024f, 1, i/(i+8f));	// Increment by color value up until max value
        }

        // starting point: row = 0
        // termination condition: row<height
        // advancement strategy: row++
        for (int row = 0; row < height; row++) {		// Sweep the picture by row
            for (int col = 0; col < width; col++) {		// Sweep the picture by column. Once every column is sweeped, the row 
                double c_re = (col - width/2)*4.0/width;	// is increased by one. 
                double c_im = (row - height/2)*4.0/width;
                double x = 0, y = 0;
                double r2;
                int iteration = 0;				// Starts iteration at 0.
                while (x*x+y*y < 4 && iteration < max) {	// Starts while loop to iterate for every parametric value
                    double x_new = x*x-y*y+c_re;
                    y = 2*x*y+c_im;				// Imaginary value for y
                    x = x_new;					// Real value for x
                    iteration++;				// Increases iteration by 1
                }
                if (iteration < max) image.setRGB(col, row, colors[iteration]);	// If iteration is below max value. rgb value is set
                else image.setRGB(col, row, black);				// Otherwise, the rgb value is 0
            }
        }

        ImageIO.write(image, "png", new File("f1024mandelbrot-color.png")); 	// Outputs the resulting image to a file
    }
}
