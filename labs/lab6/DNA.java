//====================================================
//Honor Code: The work we are submitting is a result of our own thinking and efforts.
//Kadeem LaFargue & Ryan Sayko
//CMPSC 111 Spring 2017
//Lab #6
//Date: 02 27 2017
//
//Purpose: 	This program manipulates an input DNA strand from
//		the user using the letters A, T, C, and G. For
//		clarity, the program changes the input to all
//		upper case letters. The program uses dummy letters
//		to change A to T and vice versa, and C to G and vice
//		versa. The result of this manipulation gives the complementary
//		strand of DNA. The program then manipulates the input 
//		strand from the user in three different fashions: 

//		1) A random number generator generates one of the 
//		four letters and inserts the letter at a randomly 
//		selected location using the substring method, 
//		varied within the length of the input strand. 
//
//		2) A random number generator selects the location 
//		of a removal of a letter and uses the substring 
//		function to concatenate the strand, excluding the
//		location indicating the letter's position. If the 
//		location is at 0, then the letter at the first 
//		position will be removed and the substring 
//		method will be made to go from the second letter
//		onwards.
//
//		3) Two random number generators will generate
//		the location of the letter to be replaced and
//		the letter that will replace the removed letter.
//		The replace method is used to change the letter
//		in the input strand. 
//====================================================
import java.util.Scanner;		// Import scanner utility
import java.util.Date;			// Import date utility
import java.util.Random;		// Import random number generator

public class DNA
{
	//------------------------------------
	// Program execution starts here:
	//------------------------------------
	public static void main(String[] args)
	{
		//------------------------------------
		// Label output with name and date:
		//------------------------------------
		System.out.println("Kadeem LaFargue & Ryan Sayko\nLab 6\n" + new Date() + "\n");
		Scanner scan = new Scanner(System.in); // Create scanner for user input

		//------------------------------------
		// Variable dictionary:
		//------------------------------------
		Random r = new Random(); 			// Random number generator
		String dnaString, dnaString2, compString; 	// dnaString = input by user 
								// dnaString2 = output of each manipulation 
								// compString = complementary dna strand
		int len; 					// Length of input DNA strand
		int location, location2, location3; 		// Position in the DNA strand:
								// location = randomly generated for insertion
								// location2 = randomly generated for deletion
								// location3 = randomly generated for replacement
		char q, s, u; 					// Letter chosen randomly from string:
								// q = what letter to add into DNA strand
								// s = what letter to remove in DNA strand
								// u = what letter to replace removed letter in DNA strand
		//------------------------------------
		// Input DNA strand from user:
		//------------------------------------
		System.out.println("Enter in a string of DNA using ATCG.");
		dnaString = scan.next();					// Scan input DNA strand
		len = dnaString.length();					// Finds length of input strand
		dnaString = dnaString.toUpperCase();				// Changes to uppercase for neatness
		System.out.println("Your DNA strand is: " + dnaString);		// Outputs user's DNA strand
	
		//------------------------------------
		// Find complementary DNA strand:
		//------------------------------------
		compString = dnaString.replace('T','B');			// Uses dummy letters to switch A and T, outputs result
		compString = compString.replace('A','T');
		compString = compString.replace('B','A');
		System.out.println("We replaced 'T' with 'A' and 'A' with 'T' to get: " + compString);
		compString = compString.replace('C','D');			// Uses dummy letters to switch C and G, outputs result
		compString = compString.replace('G','C');
		compString = compString.replace('D','G');
		System.out.println("We replaced 'C' with 'G' and 'G' with 'C' to get our complementary strand: " + compString);

		//------------------------------------
		// First manipulation - add letter:
		//------------------------------------
		q = "ATCG".charAt(r.nextInt(4));							// Finds random letter to add
		location = r.nextInt(len+1);								// Finds location to add letter
		dnaString2 = dnaString.substring(0,location) + q + dnaString.substring(location);	// Substring adds letter, output
		location = location + 1;								// Proper output format
		System.out.println("Inserting " + q + " at location " + location + " gives: " + dnaString2);

		//------------------------------------
		// Second manipulation - remove letter:
		//------------------------------------
		location2 = r.nextInt(len);								// Finds location of letter to remove
		if (location2 > 0)									// If statement in case that location 
		{											// is 0, so program does not crash
			dnaString2 = dnaString.substring(0,location2-1) + dnaString.substring(location2);
			System.out.println("Removing from location " + location2 + " gives: " + dnaString2);
		}
		else
		{
			dnaString2 = dnaString.substring(1);
			location2 = location2 + 1;
			System.out.println("Removing from " + location2 + " gives: " + dnaString2);
		}
		//------------------------------------
		// Third manipulation - replace letter:
		//------------------------------------
		location3 = r.nextInt(len);								// Finds location of letter to replace
		s = dnaString.charAt(location3);							// Generates letter located in location3
		u = "ATCG".charAt(r.nextInt(4));							// Generates random letter to replace
		dnaString2 = dnaString.replace(s,u);							// Replace method used to change letters
		location3 = location3 + 1;								// Proper output format
		System.out.println("We replace " + s + " with " + u + " at location " + location3 + " to get: " + dnaString2);	
	}
}
