//**********************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Ryan Sayko
// CMPSC 111 Spring 2017
// Lab #3
// Date: Feb 9 2017
//
// Purpose: This program serves as a source of shapes and colors that 
//		Lab4.java uses to create the art. These shapes and 
//		colors represent a smiley face with a top hat. One thing 
//		to note is the bottom layer of the shapes represents
//		the first few lines of code representing the shape. 
//		For example, the face represents the bottom layer of the
//		picture. Everything that is coded after the face will
//		be stacked on top of the orange circle. 
//**********************************************************

import java.awt.*;
import javax.swing.JApplet;

public class Lab4 extends JApplet
{
  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
  public void paint(Graphics page)
  {
	final int MIDX = 600;
	final int MIDY = 400;

	setBackground(Color.white);

	page.setColor(Color.orange);		// Face
	page.fillOval(MIDX/2,MIDY/2,MIDX,MIDX);

	page.setColor(Color.white);
	page.fillOval(450,300,90,75);		// Eyes
	page.fillOval(675,300,90,75);
	page.setColor(Color.green);
	page.fillOval(460,325,70,50);
	page.fillOval(685,325,70,50);
	page.setColor(Color.black);
	page.fillOval(475,335,40,40);
	page.fillOval(700,335,40,40);

	page.setColor(Color.black);
	page.drawLine(MIDX,450,500,600);		// Nose
	page.drawLine(500,600,MIDX,600);

	page.drawArc(400,550,400,200,200,130);	// Smile

	page.fillRect(300,175,600,100);		// Top Hat
	page.fillRect(450,0,300,200);

  }
}
