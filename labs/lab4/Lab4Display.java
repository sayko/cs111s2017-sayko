
//**********************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Ryan Sayko
// CMPSC 111 Spring 2017
// Lab #3
// Date: Feb 9 2017
//
// Purpose: The goal of this program is to create some art using Lab4Display.java
//		as the canvas and extracts shapes from that program. This program 
//		also sets the output as a visible picture and sets the size to 
//		1200 by 800 pixels. 
//**********************************************************
import javax.swing.*;

public class Lab4Display
{
  public static void main(String[] args)
  {
    JFrame window = new JFrame(" Ryan Sayko ");

    // Add the drawing canvas and do necessary things to
    // make the window appear on the screen!
    window.getContentPane().add(new Lab4());
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setVisible(true);
    window.setSize(1200, 800);

  }
}

